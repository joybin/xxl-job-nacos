# xxl-job-discovery 
 任务调度中心
#### 介绍
基于 `xxl-job version-2.1.1` 
以nacos服务注册中心实现服务自动发现的分布式定时任务

#### 软件环境
````
1. JDK1.8+
2. springboot2.x+
3. nacos 0.2.7
4. mysql 5.7+
````

#### 使用说明
1. 导入数据库sql
2. 根据需要修改对应properties属性
3. 输入网页地址 `http://ip:port` 
4. 根据需要修改测试用例属性 将示例执行器启动
5. 调度中心新增执行器 30s内`执行器`会自动发现`调度器`

##
原版项目地址：https://github.com/xuxueli/xxl-job
原版项目文档地址：https://www.xuxueli.com/xxl-job/
##


