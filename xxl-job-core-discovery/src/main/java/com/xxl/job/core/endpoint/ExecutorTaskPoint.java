package com.xxl.job.core.endpoint;

import com.xxl.job.core.biz.ExecutorBiz;
import com.xxl.job.core.biz.impl.ExecutorBizImpl;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.biz.model.TriggerParam;
import com.xxl.job.core.util.DiscoveryUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


/**
 * 任务执行入口
 * @author joybin
 */
@RestController
public class ExecutorTaskPoint {

    private static ExecutorBiz executorBiz = new ExecutorBizImpl();

    /**
     * 执行任务请求
     * @param triggerParam execution param
     * @return success | fail
     */
    @PostMapping("/v1/executor/api")
    public ReturnT<String> run(@RequestBody TriggerParam triggerParam, HttpServletRequest httpServletRequest) {
        String remoteHost = httpServletRequest.getRemoteHost();
        return !DiscoveryUtil.hostExist(remoteHost) ? ReturnT.FAIL : executorBiz.run(triggerParam);
    }

    /**
     * 添加任务至控制台服务列表
     * @param adminName console name
     */
    @PostMapping("/v1/executor/list")
    public void addServices(@RequestParam String adminName){
        DiscoveryUtil.addList(adminName);
    }

}
