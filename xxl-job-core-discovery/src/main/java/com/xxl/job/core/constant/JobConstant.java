package com.xxl.job.core.constant;

import com.xxl.job.core.biz.AdminBiz;

/**
 * @Description: 默认常量配置类
 * @Author joybin
 * @Date 2020/7/9
 **/
public class JobConstant {

    // 执行器执行接口
    public final static String executorApi = "/v1/executor/api";

    public final static String adminList = "/v1/executor/list";

    public final static String callBackApi =  "/api";

}
